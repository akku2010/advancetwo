import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import * as moment from 'moment';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-add-reminder',
  templateUrl: 'add-reminder.html',
})
export class AddReminderPage implements OnInit {
  islogin: any;
  portstemp: any[] = [];
  selectedVehicle: any;
  reminderTypes: any = [];

  notifTypes: any = [];
  reminderType: any;
  notifType: any;
  remDate: any;
  priorDay: number = 0;
  note: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private apiCall: ApiServiceProvider,
    private toastCtrl: ToastController,
    private translate: TranslateService
  ) {
    this.remDate = moment().format();
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);

    if (navParams.get('param') !== null) {
      this.selectedVehicle = navParams.get('param');
    }

    this.reminderTypes = [{
      viewValue: this.translate.instant("Service"),
      value: "Service",
    }, {
      viewValue: this.translate.instant("Oil Changed"),
      value: "oil_change",
    }, {
      viewValue: this.translate.instant("Tyres"),
      value: "Tyres",
    }, {
      viewValue: this.translate.instant("Maintenance"),
      value: "Maintenance",
    },
    {
      viewValue: this.translate.instant("Others"),
      value: "Others",
    }
    ];

    this.notifTypes = [
      { view: this.translate.instant("SMS"), value: "SMS" }, { view: this.translate.instant("EMAIL_1"), value: "EMAIL" }, { view: this.translate.instant("PUSH NOTIFICATION"), value: "PUSH_NOTIFICATION" }
    ]
  }

  ngOnInit() {
    this.getdevices();
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter AddReminderPage');
  }

  onChange(key) {
    console.log(key)
  }

  onDismiss() {
    this.viewCtrl.dismiss();
  }

  getdevices() {
    var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apiCall.startLoading().present();
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.portstemp = data.devices;
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  onSeletChange() {

  }

  onSeletChange1() {

  }

  addReminder() {
    if (this.notifType == undefined || this.reminderType == undefined || this.selectedVehicle == undefined) {
      this.showToast(this.translate.instant('Please fill all the mandatory fields..'));
      return;
    }
    var url = this.apiCall.mainUrl + 'reminder/addReminder';
    var payload = {};
    if (this.notifType === 'SMS') {
      payload = {
        "created_by": this.islogin._id,
        "user": this.selectedVehicle.user,
        "device": this.selectedVehicle._id,
        "reminder_type": this.reminderType,
        "notification_type": {
          "SMS": true,
          "EMAIL": false,
          "PUSH_NOTIFICATION": false
        },
        "reminder_date": new Date(this.remDate).toISOString(),
        "prior_reminder": this.priorDay,
        "note": this.note
      }
    } else if (this.notifType === 'EMAIL') {
      payload = {
        "created_by": this.islogin._id,
        "user": this.selectedVehicle.user,
        "device": this.selectedVehicle._id,
        "reminder_type": this.reminderType,
        "notification_type": {
          "SMS": false,
          "EMAIL": true,
          "PUSH_NOTIFICATION": false
        },
        "reminder_date": new Date(this.remDate).toISOString(),
        "prior_reminder": this.priorDay,
        "note": this.note
      }
    } else if (this.notifType === 'PUSH_NOTIFICATION') {
      payload = {
        "created_by": this.islogin._id,
        "user": this.selectedVehicle.user,
        "device": this.selectedVehicle._id,
        "reminder_type": this.reminderType,
        "notification_type": {
          "SMS": false,
          "EMAIL": false,
          "PUSH_NOTIFICATION": true
        },
        "reminder_date": new Date(this.remDate).toISOString(),
        "prior_reminder": this.priorDay,
        "note": this.note
      }
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(url, payload)
      .subscribe(resData => {
        this.apiCall.stopLoading();
        console.log("Resp Data=> ", resData);
        if (resData.message === 'Data Saved') {
          this.showToast('Reminder added successfully..');
          this.viewCtrl.dismiss();
        }
      },
        err => {
          this.apiCall.stopLoading();
        });
  }

  showToast(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    }).present();
  }

}
